<?php

namespace farrosoft\wpinstaller\commands;

use farrosoft\wpinstaller\Executor;

require_once( __DIR__ . '/../executor.php' );

class Post {
    private $post_id;

    private function __construct( $args ) {
        $this->post_id = Executor::runCommand( $this->getCreatePostCommand( $args ) );
    }

    private function getCreatePostCommand( $args ) {
        $command_line =
            'post create' .
            ' --post_status="publish"';
        if ( $args['post_type'] ) {
            $command_line .= ' --post_type="' . $args['post_type'] . '"';
        } else {
            $command_line .= ' --post_type="post"';
        }
        $command_line .=
            ' --porcelain';


        if ( $args['title'] ) {
            $command_line .= ' --post_title="' . $args['title'] . '"';
        }

        return $command_line;
    }

    /**
     * @param title
     * @param post_type
     *
     */
    public static function createPost( $args ) {
        $post = new Post( $args );
    }

}